/* eslint-disable no-console */
import React, { Component } from "react";
import moment from "moment";
import { Container, Row, Col } from "reactstrap";
import Timeline, {
  TimelineMarkers,
  TimelineHeaders,
  TodayMarker,
  CustomMarker,
  SidebarHeader,
  DateHeader,
} from "react-calendar-timeline";

import arrayMove from "array-move";

var minTime = moment()
  .add(-6, "months")
  .valueOf();
var maxTime = moment()
  .add(6, "months")
  .valueOf();

/* var keys = {
  groupIdKey: "id",
  groupTitleKey: "title",
  groupRightTitleKey: "rightTitle",
  itemIdKey: "id",
  itemTitleKey: "title",
  itemDivTitleKey: "title",
  itemGroupKey: "group",
  itemTimeStartKey: "start",
  itemTimeEndKey: "end"
}; */

const groups = [
  {
    id: 1,
    title: "Example task 1",
    team: [
      { avatar: "https://i.pravatar.cc/50?img=1" },
      { avatar: "https://i.pravatar.cc/50?img=2" },
      { avatar: "https://i.pravatar.cc/50?img=3" },
    ],
  },
  {
    id: 2,
    title: "Example task 2",
    team: [
      { avatar: "https://i.pravatar.cc/50?img=4" },
      { avatar: "https://i.pravatar.cc/50?img=5" },
      { avatar: "https://i.pravatar.cc/50?img=6" },
    ],
  },
  {
    id: 3,
    title: "Example task 3",
  },
];

const items = [
  {
    id: 1,
    group: 1,
    title: "item 1",
    start_time: moment(),
    end_time: moment().add(4, "d"),
    itemProps: {
      className: "bg-primary",
    },
  },
  {
    id: 2,
    group: 2,
    title: "item 2",
    start_time: moment().add(1, "d"),
    end_time: moment().add(3, "d"),
    itemProps: {
      className: "bg-warning",
    },
  },
  {
    id: 3,
    group: 3,
    title: "item 3",
    start_time: moment().add(2, "d"),
    end_time: moment().add(5, "d"),
    itemProps: {
      className: "bg-success",
    },
  },
];

const today = parseInt(moment().format("x"));
const tomorrow = parseInt(
  moment()
    .add(1, "d")
    .format("x")
);
const nextFriday = parseInt(
  moment()
    .day(5)
    .format("x")
);

const defaultTimeStart = moment()
  .add(-7, "d")
  .toDate();

const defaultTimeEnd = moment()
  .add(7, "d")
  .toDate();

const markerDates = [
  { date: today, id: 1 },
  { date: tomorrow, id: 2 },
  { date: nextFriday, id: 3 },
];

export default class App extends Component {
  constructor(props) {
    super(props);

    // const { groups, items } = generateFakeData();

    this.state = {
      groups,
      items,
      defaultTimeStart,
      defaultTimeEnd,
    };
  }

  handleCanvasClick = (groupId, time) => {
    console.log("Canvas clicked", groupId, moment(time).format());
  };

  handleCanvasDoubleClick = (groupId, time) => {
    console.log("Canvas double clicked", groupId, moment(time).format());
  };

  handleCanvasContextMenu = (group, time) => {
    console.log("Canvas context menu", group, moment(time).format());
  };

  handleItemClick = (itemId, _, time) => {
    console.log("Clicked: " + itemId, moment(time).format());
  };

  handleItemSelect = (itemId, _, time) => {
    console.log("Selected: " + itemId, moment(time).format());
  };

  handleItemDoubleClick = (itemId, _, time) => {
    console.log("Double Click: " + itemId, moment(time).format());
  };

  handleItemContextMenu = (itemId, _, time) => {
    console.log("Context Menu: " + itemId, moment(time).format());
  };

  handleItemMove = (itemId, dragTime, newGroupOrder) => {
    const { items, groups } = this.state;

    const group = groups[newGroupOrder];

    this.setState({
      items: items.map((item) =>
        item.id === itemId
          ? Object.assign({}, item, {
              start: dragTime,
              end: dragTime + (item.end - item.start),
              group: group.id,
            })
          : item
      ),
    });

    console.log("Moved", itemId, dragTime, newGroupOrder);
  };

  handleItemResize = (itemId, time, edge) => {
    const { items } = this.state;

    this.setState({
      items: items.map((item) =>
        item.id === itemId
          ? Object.assign({}, item, {
              start: edge === "left" ? time : item.start,
              end: edge === "left" ? item.end : time,
            })
          : item
      ),
    });

    console.log("Resized", itemId, time, edge);
  };

  // this limits the timeline to -6 months ... +6 months
  handleTimeChange = (visibleTimeStart, visibleTimeEnd, updateScrollCanvas) => {
    if (visibleTimeStart < minTime && visibleTimeEnd > maxTime) {
      updateScrollCanvas(minTime, maxTime);
    } else if (visibleTimeStart < minTime) {
      updateScrollCanvas(
        minTime,
        minTime + (visibleTimeEnd - visibleTimeStart)
      );
    } else if (visibleTimeEnd > maxTime) {
      updateScrollCanvas(
        maxTime - (visibleTimeEnd - visibleTimeStart),
        maxTime
      );
    } else {
      updateScrollCanvas(visibleTimeStart, visibleTimeEnd);
    }
  };

  handleOnSortEnd = ({ oldIndex, newIndex }) => {
    console.log(oldIndex, newIndex);
    this.setState({
      groups: arrayMove(this.state.groups, oldIndex, newIndex),
    });
  };

  moveResizeValidator = (action, item, time) => {
    if (time < new Date().getTime()) {
      var newTime =
        Math.ceil(new Date().getTime() / (15 * 60 * 1000)) * (15 * 60 * 1000);
      return newTime;
    }

    return time;
  };

  renderItem = (group) => (
    <div className="d-flex justify-content-between align-items-center">
      <div>{group.title}</div>
      <ul className="list-unstyled d-flex m-0">
        {typeof group.team !== "undefined" && group.team.length > 0 ? (
          group.team.map((member, index) => (
            <li
              className="rounded-circle border"
              style={{
                width: "2rem",
                height: "2rem",
                backgroundImage: `url('${member.avatar}')`,
                backgroundPosition: "center",
                backgroundSize: "cover",
                marginLeft: "-0.75rem",
              }}
              key={`member-${index}`}
            />
          ))
        ) : (
          <li
            className="rounded-circle border d-flex justify-content-center align-items-center"
            style={{
              width: "2rem",
              height: "2rem",
            }}
          >
            +
          </li>
        )}
      </ul>
    </div>
  );

  render() {
    const { groups, items, defaultTimeStart, defaultTimeEnd } = this.state;

    return (
      <Container fluid className="mt-5 p-0">
        <Row>
          <Col md={1} className="px-5">
            airnip sidebar
          </Col>
          <Col md={11}>
            <Timeline
              dragSnap={60 * 60 * 24 * 1000}
              minZoom={0}
              maxZoom={60 * 60 * 24 * 1000}
              groups={groups}
              items={items}
              lineHeight={70}
              sidebarWidth={250}
              sidebarContent={<div>Above The Left</div>}
              canMove
              canResize="right"
              canSelect
              itemsSorted
              itemTouchSendsClick={false}
              stackItems
              itemHeightRatio={0.75}
              defaultTimeStart={defaultTimeStart}
              defaultTimeEnd={defaultTimeEnd}
              onCanvasClick={this.handleCanvasClick}
              onCanvasDoubleClick={this.handleCanvasDoubleClick}
              onCanvasContextMenu={this.handleCanvasContextMenu}
              onItemClick={this.handleItemClick}
              onItemSelect={this.handleItemSelect}
              onItemContextMenu={this.handleItemContextMenu}
              onItemMove={this.handleItemMove}
              onItemResize={this.handleItemResize}
              onItemDoubleClick={this.handleItemDoubleClick}
              onTimeChange={this.handleTimeChange}
              moveResizeValidator={this.moveResizeValidator}
              useResizeHandle={true}
              groupRenderer={({ group }) => this.renderItem(group)}
              // any other React Sortable HOC props here
              // onSortEnd={this.handleOnSortEnd}
              lockAxis="y"
              pressDelay={300}
            >
              <TimelineHeaders>
                <SidebarHeader>
                  {({ data, getRootProps }) => (
                    <div
                      {...getRootProps()}
                      className="d-flex justify-content-between align-items-center p-3"
                    >
                      <div>Tasks</div>
                      <button className="btn btn-primary ml-3">+</button>
                    </div>
                  )}
                </SidebarHeader>
                <DateHeader unit="month" />
                <DateHeader unit="day" />
              </TimelineHeaders>
              <TimelineMarkers>
                <TodayMarker />
                {markerDates.map((marker) => (
                  <CustomMarker key={marker.date} date={marker.date} />
                ))}
              </TimelineMarkers>
            </Timeline>
          </Col>
        </Row>
      </Container>
    );
  }
}
