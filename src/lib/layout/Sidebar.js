import PropTypes from "prop-types";
import React, { Component } from "react";
import { SortableContainer, SortableElement } from "react-sortable-hoc";

import { _get, arraysEqual } from "../utility/generic";

const SortableItem = SortableElement(({ children }) => {
  return (
    <li className="sortable-item" style={{ listStyle: "none" }}>
      {children}
    </li>
  );
});

const SortableList = SortableContainer(
  ({
    groups,
    groupHeights,
    isRightSidebar,
    keys: { groupIdKey, groupTitleKey, groupRightTitleKey },
    renderGroupContent,
  }) => {
    return (
      <ul
        className="sortable-list"
        style={{ margin: "unset", padding: "unset" }}
      >
        {groups.map((group, index) => {
          const elementStyle = {
            height: `${groupHeights[index]}px`,
            lineHeight: `${groupHeights[index]}px`,
          };

          return (
            <SortableItem
              key={_get(group, groupIdKey)}
              index={index}
              sortIndex={index}
            >
              <div
                className={
                  "rct-sidebar-row rct-sidebar-row-" +
                  (index % 2 === 0 ? "even" : "odd")
                }
                style={elementStyle}
              >
                {renderGroupContent({
                  group,
                  isRightSidebar,
                  groupTitleKey,
                  groupRightTitleKey,
                })}
              </div>
            </SortableItem>
          );
        })}
      </ul>
    );
  }
);

export default class Sidebar extends Component {
  static propTypes = {
    groups: PropTypes.oneOfType([PropTypes.array, PropTypes.object]).isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    groupHeights: PropTypes.array.isRequired,
    keys: PropTypes.object.isRequired,
    groupRenderer: PropTypes.func,
    isRightSidebar: PropTypes.bool,
  };

  shouldComponentUpdate(nextProps) {
    return !(
      nextProps.keys === this.props.keys &&
      nextProps.width === this.props.width &&
      nextProps.height === this.props.height &&
      arraysEqual(nextProps.groups, this.props.groups) &&
      arraysEqual(nextProps.groupHeights, this.props.groupHeights)
    );
  }

  renderGroupContent({
    group,
    isRightSidebar,
    groupTitleKey,
    groupRightTitleKey,
  }) {
    const { groupRenderer } = this.props;

    if (groupRenderer) {
      return React.createElement(groupRenderer, {
        group,
        isRightSidebar,
      });
    } else {
      return _get(group, isRightSidebar ? groupRightTitleKey : groupTitleKey);
    }
  }

  renderUnsortableGroupContent() {
    const {
      groups,
      isRightSidebar,
      groupHeights,
      groupTitleKey,
      groupRightTitleKey,
    } = this.props;

    return groups.map((group, index) => {
      const elementStyle = {
        height: `${groupHeights[index]}px`,
        lineHeight: `${groupHeights[index]}px`,
      };

      return (
        <div
          className={
            "rct-sidebar-row rct-sidebar-row-" +
            (index % 2 === 0 ? "even" : "odd")
          }
          style={elementStyle}
          key={`rct-sidebar-row-${index}`}
        >
          {this.renderGroupContent({
            group,
            isRightSidebar,
            groupTitleKey,
            groupRightTitleKey,
          })}
        </div>
      );
    });
  }

  render() {
    const { width, height, isRightSidebar } = this.props;

    const sidebarStyle = {
      width: `${width}px`,
      height: `${height}px`,
    };

    const groupsStyle = {
      width: `${width}px`,
    };

    return (
      <div
        className={"rct-sidebar" + (isRightSidebar ? " rct-sidebar-right" : "")}
        style={sidebarStyle}
      >
        <div style={groupsStyle}>
          {console.log("WTF", this.props)}
          {this.props.onSortEnd ? (
            <SortableList
              {...this.props}
              renderGroupContent={this.renderGroupContent.bind(this)}
            />
          ) : (
            this.renderUnsortableGroupContent()
          )}
        </div>
      </div>
    );
  }
}
