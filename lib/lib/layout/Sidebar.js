"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

var _react = _interopRequireWildcard(require("react"));

var _reactSortableHoc = require("react-sortable-hoc");

var _generic = require("../utility/generic");

function _getRequireWildcardCache() { if (typeof WeakMap !== "function") return null; var cache = new WeakMap(); _getRequireWildcardCache = function _getRequireWildcardCache() { return cache; }; return cache; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var SortableItem = (0, _reactSortableHoc.SortableElement)(function (_ref) {
  var children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement("li", {
    className: "sortable-item",
    style: {
      listStyle: "none"
    }
  }, children);
});
var SortableList = (0, _reactSortableHoc.SortableContainer)(function (_ref2) {
  var groups = _ref2.groups,
      groupHeights = _ref2.groupHeights,
      isRightSidebar = _ref2.isRightSidebar,
      _ref2$keys = _ref2.keys,
      groupIdKey = _ref2$keys.groupIdKey,
      groupTitleKey = _ref2$keys.groupTitleKey,
      groupRightTitleKey = _ref2$keys.groupRightTitleKey,
      renderGroupContent = _ref2.renderGroupContent;
  return /*#__PURE__*/_react["default"].createElement("ul", {
    className: "sortable-list",
    style: {
      margin: "unset",
      padding: "unset"
    }
  }, groups.map(function (group, index) {
    var elementStyle = {
      height: "".concat(groupHeights[index], "px"),
      lineHeight: "".concat(groupHeights[index], "px")
    };
    return /*#__PURE__*/_react["default"].createElement(SortableItem, {
      key: (0, _generic._get)(group, groupIdKey),
      index: index,
      sortIndex: index
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: "rct-sidebar-row rct-sidebar-row-" + (index % 2 === 0 ? "even" : "odd"),
      style: elementStyle
    }, renderGroupContent({
      group: group,
      isRightSidebar: isRightSidebar,
      groupTitleKey: groupTitleKey,
      groupRightTitleKey: groupRightTitleKey
    })));
  }));
});

var Sidebar = /*#__PURE__*/function (_Component) {
  _inherits(Sidebar, _Component);

  var _super = _createSuper(Sidebar);

  function Sidebar() {
    _classCallCheck(this, Sidebar);

    return _super.apply(this, arguments);
  }

  _createClass(Sidebar, [{
    key: "shouldComponentUpdate",
    value: function shouldComponentUpdate(nextProps) {
      return !(nextProps.keys === this.props.keys && nextProps.width === this.props.width && nextProps.height === this.props.height && (0, _generic.arraysEqual)(nextProps.groups, this.props.groups) && (0, _generic.arraysEqual)(nextProps.groupHeights, this.props.groupHeights));
    }
  }, {
    key: "renderGroupContent",
    value: function renderGroupContent(_ref3) {
      var group = _ref3.group,
          isRightSidebar = _ref3.isRightSidebar,
          groupTitleKey = _ref3.groupTitleKey,
          groupRightTitleKey = _ref3.groupRightTitleKey;
      var groupRenderer = this.props.groupRenderer;

      if (groupRenderer) {
        return /*#__PURE__*/_react["default"].createElement(groupRenderer, {
          group: group,
          isRightSidebar: isRightSidebar
        });
      } else {
        return (0, _generic._get)(group, isRightSidebar ? groupRightTitleKey : groupTitleKey);
      }
    }
  }, {
    key: "renderUnsortableGroupContent",
    value: function renderUnsortableGroupContent() {
      var _this = this;

      var _this$props = this.props,
          groups = _this$props.groups,
          isRightSidebar = _this$props.isRightSidebar,
          groupHeights = _this$props.groupHeights,
          groupTitleKey = _this$props.groupTitleKey,
          groupRightTitleKey = _this$props.groupRightTitleKey;
      return groups.map(function (group, index) {
        var elementStyle = {
          height: "".concat(groupHeights[index], "px"),
          lineHeight: "".concat(groupHeights[index], "px")
        };
        return /*#__PURE__*/_react["default"].createElement("div", {
          className: "rct-sidebar-row rct-sidebar-row-" + (index % 2 === 0 ? "even" : "odd"),
          style: elementStyle,
          key: "rct-sidebar-row-".concat(index)
        }, _this.renderGroupContent({
          group: group,
          isRightSidebar: isRightSidebar,
          groupTitleKey: groupTitleKey,
          groupRightTitleKey: groupRightTitleKey
        }));
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props2 = this.props,
          width = _this$props2.width,
          height = _this$props2.height,
          isRightSidebar = _this$props2.isRightSidebar;
      var sidebarStyle = {
        width: "".concat(width, "px"),
        height: "".concat(height, "px")
      };
      var groupsStyle = {
        width: "".concat(width, "px")
      };
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "rct-sidebar" + (isRightSidebar ? " rct-sidebar-right" : ""),
        style: sidebarStyle
      }, /*#__PURE__*/_react["default"].createElement("div", {
        style: groupsStyle
      }, console.log("WTF", this.props), this.props.onSortEnd ? /*#__PURE__*/_react["default"].createElement(SortableList, _extends({}, this.props, {
        renderGroupContent: this.renderGroupContent.bind(this)
      })) : this.renderUnsortableGroupContent()));
    }
  }]);

  return Sidebar;
}(_react.Component);

exports["default"] = Sidebar;

_defineProperty(Sidebar, "propTypes", {
  groups: _propTypes["default"].oneOfType([_propTypes["default"].array, _propTypes["default"].object]).isRequired,
  width: _propTypes["default"].number.isRequired,
  height: _propTypes["default"].number.isRequired,
  groupHeights: _propTypes["default"].array.isRequired,
  keys: _propTypes["default"].object.isRequired,
  groupRenderer: _propTypes["default"].func,
  isRightSidebar: _propTypes["default"].bool
});